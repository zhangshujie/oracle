create user lyt identified by 123456;
grant connect, resource to lyt;
grant unlimited tablespace  to lyt;
CREATE TABLESPACE lyt_SYSTEM
DATAFILE '/home/oracle/app/oracle/oradata/orcl/OracleSystem01.dbf'
SIZE 500M
  4  AUTOEXTEND ON NEXT 100M;

CREATE TABLESPACE lyt_DATA
DATAFILE '/home/oracle/app/oracle/oradata/orcl/OracleUserdata01.dbf'
SIZE 500M
  4  AUTOEXTEND ON NEXT 100M;