CREATE OR REPLACE PACKAGE order_management_pkg AS

-- 新增订单
PROCEDURE new_order(customer_id IN NUMBER, total_amount IN NUMBER, details_list IN xmltype DEFAULT NULL);

-- 删除订单
PROCEDURE delete_order(order_id IN NUMBER);

-- 查询订单详情
PROCEDURE get_order_detail(order_id IN NUMBER);

FUNCTION convert_to_xml(p_recordset IN sys_refcursor)
   RETURN xmltype;

END order_management_pkg;
/

CREATE OR REPLACE PACKAGE BODY order_management_pkg AS

-- 新增订单
PROCEDURE new_order(customer_id IN NUMBER, total_amount IN NUMBER, details_list IN xmltype DEFAULT NULL) IS 
 l_order_id NUMBER;
BEGIN
 SELECT NVL(MAX(order_id),0)+1 INTO l_order_id FROM orderss;
 INSERT INTO orderss (order_id, customer_id, order_date, total_amount) VALUES (l_order_id, customer_id, SYSDATE, total_amount);
 IF details_list IS NOT NULL THEN
   FOR i IN (
     SELECT EXTRACTVALUE(VALUE(t), '/product_id') AS product_id,
            EXTRACTVALUE(VALUE(t), '/quantity') AS quantity,
            EXTRACTVALUE(VALUE(t), '/unit_price') AS unit_price
      FROM TABLE(XMLSEQUENCE(EXTRACT(details_list, '/details/item'))) t
   ) LOOP
     INSERT INTO order_details (order_detail_id, order_id, product_id, quantity, price)
     VALUES ((SELECT NVL(MAX(order_detail_id),0)+1 FROM order_details),
             l_order_id,
             i.product_id,
             i.quantity,
             i.unit_price);
    END LOOP;
 END IF;
 COMMIT;
END new_order;

-- 删除订单及明细 
PROCEDURE delete_order(order_id IN NUMBER) IS 
BEGIN
  DELETE FROM order_details WHERE order_id = order_id;
  DELETE FROM orderss WHERE order_id = order_id;
  COMMIT;
END delete_order;


-- 查询订单详情
PROCEDURE get_order_detail(order_id IN NUMBER) IS 
 v_cursor sys_refcursor;
BEGIN
   OPEN v_cursor FOR
   WITH order_tbl AS (
     SELECT os.order_id, os.customer_id, c.customer_name, os.total_amount, os.order_date
       FROM orderss os INNER JOIN customers c ON os.customer_id = c.customer_id
      WHERE os.order_id = order_id
   ), detail_tbl AS (
     SELECT od.order_detail_id, od.order_id, p.product_name, od.quantity, od.price
       FROM order_details od INNER JOIN products p ON od.product_id = p.product_id
      WHERE od.order_id = order_id
     )
   SELECT XMLELEMENT("order", 
           XMLFOREST(order_tbl.order_id AS "order_id",
                     order_tbl.customer_name AS "customer_name", 
                     order_tbl.total_amount AS "total_amount",
                     to_char(order_tbl.order_date, 'YYYY/MM/DD HH24:MI:SS') AS "order_date"), 
         XMLAGG(
             XMLELEMENT("item", 
               XMLFOREST(detail_tbl.product_name AS "product_name",
                         detail_tbl.quantity AS "quantity",
                         detail_tbl.price AS "price")
                       )
                 )
             ) as order_detail_xml
     FROM order_tbl INNER JOIN detail_tbl ON order_tbl.order_id = detail_tbl.order_id;  
      DBMS_OUTPUT.PUT_LINE('Order Detail for order id ' || order_id);
      DBMS_OUTPUT.PUT_LINE(convert_to_xml(v_cursor).getclobval());
   CLOSE v_cursor;
END get_order_detail;

-- 将结果集转成XML格式
FUNCTION convert_to_xml(p_recordset IN sys_refcursor) RETURN xmltype IS 
l_ctx dbms_xmlgen.ctxhandle;
begin
  l_ctx := dbms_xmlgen.newcontext(p_recordset);
  return (dbms_xmlgen.getxmltype(l_ctx));
end convert_to_xml;

END order_management_pkg;
/
