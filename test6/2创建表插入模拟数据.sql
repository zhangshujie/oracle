    -- 创建商品表
 CREATE TABLE products (
    product_id NUMBER PRIMARY KEY,
    product_name VARCHAR2(100) NOT NULL,
    category VARCHAR2(50),
    price NUMBER(10, 2) NOT NULL,
    stock_quantity NUMBER NOT NULL,
    description VARCHAR2(500)
);

    -- 创建客户表
   CREATE TABLE customers (
    customer_id NUMBER PRIMARY KEY,
    customer_name VARCHAR2(50) NOT NULL,
    phone VARCHAR2(20) NOT NULL,
    address VARCHAR2(200)
);

    -- 创建订单表
CREATE TABLE orderss (
    order_id NUMBER PRIMARY KEY,
    customer_id NUMBER,
    order_date DATE NOT NULL,
    total_amount NUMBER(10, 2) NOT NULL,
);

    -- 创建订单详情表
CREATE TABLE order_details (
    order_detail_id NUMBER PRIMARY KEY,
    order_id NUMBER,
    product_id NUMBER,
    quantity NUMBER NOT NULL,
    price NUMBER(10, 2) NOT NULL,
);

-- 插入产品数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO products (product_id, product_name, price, stock_quantity)
    VALUES (i, 'Product' || i, ROUND(DBMS_RANDOM.VALUE(1, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 500), 0));
  END LOOP;
  COMMIT;
END;
/

-- 创建序列
CREATE SEQUENCE customer_seq START WITH 1 INCREMENT BY 1;

-- 插入客户数据
DECLARE
i NUMBER;
BEGIN
FOR i IN 1..50000 LOOP
INSERT INTO customers (customer_id, customer_name, phone, address)
VALUES (customer_seq.NEXTVAL, 'Customer Name ' || i, '123-456-' || LPAD(TRUNC(DBMS_RANDOM.VALUE(0,9999)),4,'0'), 'Address of Customer ' || i);
END LOOP;
END;
/
commit;


-- 插入订单数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO orderss (order_id, customer_id, order_date, total_amount)
    VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1,50000)), SYSDATE - FLOOR(DBMS_RANDOM.VALUE(0,365)), DBMS_RANDOM.VALUE(50,9999));
  END LOOP;
END;
/
commit;

-- 插入订单详情数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO order_details (order_detail_id, order_id, product_id, quantity, price)
    VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1,30000)), TRUNC(DBMS_RANDOM.VALUE(1,100)), TRUNC(DBMS_RANDOM.VALUE(1,10)), DBMS_RANDOM.VALUE(5,500));
  END LOOP;
END;
/
commit;