# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计 

- 学号：202010211232  姓名：张树杰  班级：2020级软件工程4班

### 实验目的

- 设计一套基于Oracle数据库的商品销售系统的数据库设计方案。
  - 表及表空间设计方案。至少两个表空间，至少4张表，总的模拟数据量不少于10万条。
  - 设计权限及用户分配方案。至少两个用户。
  - 在数据库中建立一个程序包，在包中用PL/SQL语言设计一些存储过程和函数，实现比较复杂的业务逻辑。
  - 设计一套数据库的备份方案。

### 期末考核要求

- 实验在自己的计算机上完成。
- 文档`必须提交`到你的oracle项目中的test6目录中。test6目录中必须至少有3个文件：
  - test6.md主文件。
  - 数据库创建和维护用的脚本文件*.sql。
  - [test6_design.docx](./test6_design.docx)，学校格式的完整报告。
- 文档中所有设计和数据都必须是独立完成的真实实验结果。不得抄袭，杜撰。
- 提交时间： 2023-5-26日前

### 评分标准

| 评分项|评分标准|满分|
|:-----|:-----|:-----|
|文档整体|文档内容详实、规范，美观大方|10|
|表设计|表设计及表空间设计合理，样例数据合理|20|
|用户管理|权限及用户分配方案设计正确|20|
|PL/SQL设计|存储过程和函数设计正确|30|
|备份方案|备份方案设计正确|20|

### 步骤

#### 创建用户并授权,连接数据库

``` sql
create user c##zsj identified by 123456;
grant connect, resource to c##zsj;
grant unlimited tablespace  to c##zsj;
```

![image-20230525115322103](./pict01.png) 

#### 创建表空间

```
CREATE TABLESPACE zsj_SYSTEM
DATAFILE '/home/oracle/app/oracle/oradata/orcl/OracleSystem_01.dbf'
SIZE 500M
  4  AUTOEXTEND ON NEXT 100M;

CREATE TABLESPACE zsj_DATA
DATAFILE '/home/oracle/app/oracle/oradata/orcl/OracleUserdata_01.dbf'
SIZE 500M
  4  AUTOEXTEND ON NEXT 100M;
```

![pict02](pict02.png) 

表空间结构：

| 表空间名称 | 表名称        | 表描述     |
| ---------- | :------------ | ---------- |
| zsj_SYSTEM | USERS         | 用户信息表 |
| zsj_SYSTEM | SALES_SUMMARY | 销售额表   |
| zsj_DATA   | MY_ORDERS     | 订单信息表 |
| zsj_DATA   | ORDER_DETAILS | 订单详情表 |
| zsj_DATA   | PRODUCTS      | 商品信息表 |





#### 创建相关表

1. 商品信息表

```sql
    -- 创建商品表
 CREATE TABLE products (
    product_id NUMBER PRIMARY KEY,
    product_name VARCHAR2(100) NOT NULL,
    category VARCHAR2(50),
    price NUMBER(10, 2) NOT NULL,
    stock_quantity NUMBER NOT NULL,
    description VARCHAR2(500)
);
```

![pict03](./pict03.png)

1. 客户表

```sql

    -- 创建客户表
   CREATE TABLE customers (
    customer_id NUMBER PRIMARY KEY,
    customer_name VARCHAR2(50) NOT NULL,
    phone VARCHAR2(20) NOT NULL,
    address VARCHAR2(200)
);


```

![pict04](./pict04.png)



1. 订单表

```sql

    -- 创建订单表
CREATE TABLE orderss (
    order_id NUMBER PRIMARY KEY,
    customer_id NUMBER,
    order_date DATE NOT NULL,
    total_amount NUMBER(10, 2) NOT NULL,
);
```

![pict05](./pict05.png)

1. 订单详情表

```sql


    -- 创建订单详情表
CREATE TABLE order_details (
    order_detail_id NUMBER PRIMARY KEY,
    order_id NUMBER,
    product_id NUMBER,
    quantity NUMBER NOT NULL,
    price NUMBER(10, 2) NOT NULL,
);

```

![pict06](./pict06.png)

#### 插入填充数据

插入产品数据

```sql

-- 插入产品数据
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO products (product_id, product_name, price, stock_quantity)
    VALUES (i, 'Product' || i, ROUND(DBMS_RANDOM.VALUE(1, 100), 2), ROUND(DBMS_RANDOM.VALUE(1, 500), 0));
  END LOOP;
  COMMIT;
END;
/
```

![pict07](./pict07.png)

插入客户数据

```sql
-- 创建序列
CREATE SEQUENCE customer_seq START WITH 1 INCREMENT BY 1;

-- 插入客户数据
DECLARE
i NUMBER;
BEGIN
FOR i IN 1..50000 LOOP
INSERT INTO customers (customer_id, customer_name, phone, address)
VALUES (customer_seq.NEXTVAL, 'Customer Name ' || i, '123-456-' || LPAD(TRUNC(DBMS_RANDOM.VALUE(0,9999)),4,'0'), 'Address of Customer ' || i);
END LOOP;
END;
/
commit;
```

![pict08](./pict08.png)

插入订单数据

```sql

-- 插入订单数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO orderss (order_id, customer_id, order_date, total_amount)
    VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1,50000)), SYSDATE - FLOOR(DBMS_RANDOM.VALUE(0,365)), DBMS_RANDOM.VALUE(50,9999));
  END LOOP;
END;
/
commit;
```

![pict09](./pict09.png)

插入订单详情数据

```sql

-- 插入订单详情数据
DECLARE
  i NUMBER;
BEGIN
  FOR i IN 1..50000 LOOP
    INSERT INTO order_details (order_detail_id, order_id, product_id, quantity, price)
    VALUES (i, TRUNC(DBMS_RANDOM.VALUE(1,30000)), TRUNC(DBMS_RANDOM.VALUE(1,100)), TRUNC(DBMS_RANDOM.VALUE(1,10)), DBMS_RANDOM.VALUE(5,500));
  END LOOP;
END;
/
commit;
```

![pict10](./pict10.png)

#### 设计权限及用户分配

```sql
-- 创建用户1（普通用户）
CREATE USER c##normal_USER_1 IDENTIFIED BY password;

-- 为用户1赋予权限
GRANT SELECT, INSERT ON PRODUCTS TO c##normal_USER_1;
GRANT SELECT, INSERT ON MY_ORDERS TO c##normal_USER_1;
GRANT SELECT ON USERS TO c##normal_USER_1;
GRANT SELECT ON order_details TO c##normal_USER_1;

-- 创建用户2（管理员用户）
CREATE USER c##super_USER_2 IDENTIFIED BY password;

-- 为用户2赋予权限
GRANT SELECT, UPDATE, DELETE ON PRODUCTS TO c##super_USER_2;
GRANT SELECT, UPDATE, DELETE ON MY_ORDERS TO c##super_USER_2;
GRANT SELECT, UPDATE, DELETE ON USERS TO c##super_USER_2;
GRANT SELECT ON order_details TO c##super_USER_2;
```

![pict11](./pict11.png)

#### PL/SQL设计

```sql
CREATE OR REPLACE PACKAGE order_management_pkg AS

-- 新增订单
PROCEDURE new_order(customer_id IN NUMBER, total_amount IN NUMBER, details_list IN xmltype DEFAULT NULL);

-- 删除订单
PROCEDURE delete_order(order_id IN NUMBER);

-- 查询订单详情
PROCEDURE get_order_detail(order_id IN NUMBER);

FUNCTION convert_to_xml(p_recordset IN sys_refcursor)
   RETURN xmltype;

END order_management_pkg;
/

CREATE OR REPLACE PACKAGE BODY order_management_pkg AS

-- 新增订单
PROCEDURE new_order(customer_id IN NUMBER, total_amount IN NUMBER, details_list IN xmltype DEFAULT NULL) IS 
 l_order_id NUMBER;
BEGIN
 SELECT NVL(MAX(order_id),0)+1 INTO l_order_id FROM orderss;
 INSERT INTO orderss (order_id, customer_id, order_date, total_amount) VALUES (l_order_id, customer_id, SYSDATE, total_amount);
 IF details_list IS NOT NULL THEN
   FOR i IN (
     SELECT EXTRACTVALUE(VALUE(t), '/product_id') AS product_id,
            EXTRACTVALUE(VALUE(t), '/quantity') AS quantity,
            EXTRACTVALUE(VALUE(t), '/unit_price') AS unit_price
      FROM TABLE(XMLSEQUENCE(EXTRACT(details_list, '/details/item'))) t
   ) LOOP
     INSERT INTO order_details (order_detail_id, order_id, product_id, quantity, price)
     VALUES ((SELECT NVL(MAX(order_detail_id),0)+1 FROM order_details),
             l_order_id,
             i.product_id,
             i.quantity,
             i.unit_price);
    END LOOP;
 END IF;
 COMMIT;
END new_order;

-- 删除订单及明细 
PROCEDURE delete_order(order_id IN NUMBER) IS 
BEGIN
  DELETE FROM order_details WHERE order_id = order_id;
  DELETE FROM orderss WHERE order_id = order_id;
  COMMIT;
END delete_order;


-- 查询订单详情
PROCEDURE get_order_detail(order_id IN NUMBER) IS 
 v_cursor sys_refcursor;
BEGIN
   OPEN v_cursor FOR
   WITH order_tbl AS (
     SELECT os.order_id, os.customer_id, c.customer_name, os.total_amount, os.order_date
       FROM orderss os INNER JOIN customers c ON os.customer_id = c.customer_id
      WHERE os.order_id = order_id
   ), detail_tbl AS (
     SELECT od.order_detail_id, od.order_id, p.product_name, od.quantity, od.price
       FROM order_details od INNER JOIN products p ON od.product_id = p.product_id
      WHERE od.order_id = order_id
     )
   SELECT XMLELEMENT("order", 
           XMLFOREST(order_tbl.order_id AS "order_id",
                     order_tbl.customer_name AS "customer_name", 
                     order_tbl.total_amount AS "total_amount",
                     to_char(order_tbl.order_date, 'YYYY/MM/DD HH24:MI:SS') AS "order_date"), 
         XMLAGG(
             XMLELEMENT("item", 
               XMLFOREST(detail_tbl.product_name AS "product_name",
                         detail_tbl.quantity AS "quantity",
                         detail_tbl.price AS "price")
                       )
                 )
             ) as order_detail_xml
     FROM order_tbl INNER JOIN detail_tbl ON order_tbl.order_id = detail_tbl.order_id;  
      DBMS_OUTPUT.PUT_LINE('Order Detail for order id ' || order_id);
      DBMS_OUTPUT.PUT_LINE(convert_to_xml(v_cursor).getclobval());
   CLOSE v_cursor;
END get_order_detail;

-- 将结果集转成XML格式
FUNCTION convert_to_xml(p_recordset IN sys_refcursor) RETURN xmltype IS 
l_ctx dbms_xmlgen.ctxhandle;
begin
  l_ctx := dbms_xmlgen.newcontext(p_recordset);
  return (dbms_xmlgen.getxmltype(l_ctx));
end convert_to_xml;

END order_management_pkg;
/

```

- 存储过程NEW_ORDER：该过程用于新增一条订单记录，并自动为其指定订单号(order_id)。它需要传递客户ID(customer_id)，订单总额(total_amount) 和订单中商品明细清单(details_list)参数.
- 存储过程DELETE_ORDER：该过程将根据给定订单id删除orderss和order_details表中相应的产品详情。
- 存储过程GET_ORDER_DETAILS: 该过程根据给定订单ID返回完整的订单详细清单.
- 函数CONVERT_TO_XML：All four subprograms of the package use this function to convert their output data into XML format, which can optionally be returned to callers. 这个函数接收数据集作为参数并将这些数据转换成XML格式。

![pict12](./pict12.png)

#### 数据库备份

1. 创建一个存储备份文件的目录。

```
sqlCopy CodeCREATE OR REPLACE DIRECTORY BACKUP_DIR AS '/home/oracle/app/oracle/oradata/orcl/u01/backup';
```

   2.使用 RMAN 工具进行数据库备份，并将备份文件保存到指定目录中。

- 完整备份

```
sqlCopy CodeRUN {
  ALLOCATE CHANNEL c1 DEVICE TYPE disk FORMAT '/home/oracle/app/oracle/oradata/orcl/u01/backup/full_%d_%Y%m%d_%s.bak';
  BACKUP DATABASE PLUS ARCHIVELOG;
  RELEASE CHANNEL c1;
}
```

上述语句将使用 RMAN 工具对整个数据库进行备份，包括归档日志文件。备份文件将以 full_数据库名_备份日期_备份序号.bak 的格式保存在 BACKUP_DIR 目录下。

- 增量备份

```
sqlCopy CodeRUN{
  ALLOCATE CHANNEL c1 DEVICE TYPE disk FORMAT '/home/oracle/app/oracle/oradata/orcl/u01/backup/incr_%d_%Y%m%d_%s.bak';
  BACKUP INCREMENTAL LEVEL 1 DATABASE PLUS ARCHIVELOG;
  RELEASE CHANNEL c1;
}
```

上述语句将使用 RMAN 工具对增量数据进行备份，并将备份文件保存在 BACKUP_DIR 目录下。

   3.检查备份文件的完整性和可恢复性。

```
sqlCopy CodeRUN {
  ALLOCATE CHANNEL c1 DEVICE TYPE disk;
  RESTORE DATABASE VALIDATE CHECK LOGICAL;
  RELEASE CHANNEL c1;
}
```

上述语句将检查备份文件的完整性和可恢复性，并输出检查结果。如果备份文件出现问题，需要及时重新执行备份操作。

   4.定时运行备份任务。

使用 Unix cron 或 Windows 计划任务定时运行备份任务，确保数据库的备份工作自动化和及时性

5. 数据库复制

使用数据库复制技术可以实现数据的备份和灾难恢复，同时可用于负载均衡、数据分发等功能。常见的数据库复制技术包括 Oracle Streams 和 GoldenGate。

6. 文件系统备份

可以通过备份文件系统来备份数据库。在此种场景下，需要备份数据库的所有数据文件、日志文件和配置文件等。但是该方法备份速度相对较慢，并且需要停止数据库服务来进行备份操作。

7. 在线备份

在线备份是指备份时不需要停止数据库服务，保证了生产环境的持续性。常见的在线备份软件包括 Veritas NetBackup、Commvault 和 Veeam 等。

